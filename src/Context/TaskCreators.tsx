import {
  SET_TOKEN,
  LOAD_TASKS,
  LOAD_PROJECTS,
  DELETE_TASK,
  UPDATE_TASK,
  CHANGE_DATE,
  CLEAR_STATE,
  SELECT_TASK,
  FILTER_TASKS,
  DELETE_DATE,
} from './TaskActions';

import { ActionTask, ProjectType, TaskType } from '../Utils/types';

export const setToken = (token: string): ActionTask => {
  return {
    type: SET_TOKEN,
    token,
  };
};

export const loadTasks = (tasks: TaskType[]): ActionTask => {
  return {
    type: LOAD_TASKS,
    tasks,
  };
};

export const filterTasks = (tasks: TaskType[]): ActionTask => {
  return {
    type: FILTER_TASKS,
    tasks,
  };
};

export const loadProjects = (projects: ProjectType[]) => {
  return {
    type: LOAD_PROJECTS,
    projects,
  };
};

export const deleteTask = (id: string) => {
  console.log('en el creator id: ' + id);
  return {
    type: DELETE_TASK,
    id,
  };
};

export const updateTask = (task: TaskType) => {
  return {
    type: UPDATE_TASK,
    task,
  };
};

export const changeDate = (id: string, option: string, value: string) => {
  return {
    type: CHANGE_DATE,
    id,
    option,
    value,
  };
};

export const deleteDate = (id: string) => {
  return {
    type: DELETE_DATE,
    id,
  };
};

export const clearState = () => {
  return {
    type: CLEAR_STATE,
  };
};

export const selectTask = (task: TaskType) => {
  return {
    type: SELECT_TASK,
    task,
  };
};
