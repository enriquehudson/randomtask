import React, { createContext, useReducer, useContext } from 'react';
import TasksReducer, { tasksInitialState } from './TasksReducer';
import { StateReducer, ActionTask } from '../Utils/types';

const TasksContext = createContext<{
  tasksState: StateReducer;
  tasksDispatch: React.Dispatch<ActionTask>;
}>({
  tasksState: tasksInitialState,
  tasksDispatch: function (value: ActionTask): void {
    throw new Error('Function not implemented.');
  }
});

export const useTasksContext = () => {

  const context = useContext(TasksContext);
  if (!context) {
    throw new Error('useTasksContext must be used within a TasksProvider');
  }
  return context;
}

const TasksProvider = (props: any) => {
  const [tasksState, tasksDispatch] = useReducer(
    TasksReducer,
    tasksInitialState,
  );

  const tasksContext = {
    tasksState,
    tasksDispatch,
  };

  return (
    <TasksContext.Provider value={tasksContext}>
      {props.children}
    </TasksContext.Provider>
  );
};

export default TasksProvider;
