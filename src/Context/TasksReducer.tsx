import {
  SET_TOKEN,
  LOAD_TASKS,
  LOAD_PROJECTS,
  DELETE_TASK,
  UPDATE_TASK,
  CHANGE_DATE,
  DELETE_DATE,
  CLEAR_STATE,
  FILTER_TASKS,
} from './TaskActions';
import { StateReducer, ActionTask, TaskType } from '../Utils/types';

export const tasksInitialState: StateReducer = {
  token: '',
  tasks: [],
  projects: [],
  filteredTasks: [],
  selectedTask: null,
};

const TasksReducer = (
  state: StateReducer = tasksInitialState,
  action: ActionTask,
): StateReducer => {
  switch (action.type) {
    case SET_TOKEN:
      return {
        ...state,
        token: action.token || '',
      };
    case LOAD_TASKS:
      return {
        ...state,
        tasks: action.tasks || [],
      };
    case FILTER_TASKS:
      return {
        ...state,
        filteredTasks: action.tasks || [],
      };
    case LOAD_PROJECTS:
      return {
        ...state,
        projects: action.projects || [],
      };
    case DELETE_TASK:
      return {
        ...state,
        tasks: state.tasks?.filter((task: TaskType) => task.id !== action.id),
      };
    case UPDATE_TASK:
      return {
        ...state,
      };
    case DELETE_DATE:
      return {
        ...state,
        tasks: state.tasks?.map((task: TaskType) => {
          if (task.id === action.id) {
            task.due = null;
            return task;
          }

          return task;
        }),
      };
    case CHANGE_DATE:
      return {
        ...state,
        tasks: state.tasks?.map((task: TaskType) => {
          if (task.id === action.id) {
            //task.due.date = '';
            if (action.value === '') {
              task.due = null;
              return task;
            } else {
              return {
                ...task,
                due: { ...task.due, [action.option]: action.value },
              };
            }
          }
          return task;
        }),
      };
    case CLEAR_STATE:
      return {
        ...tasksInitialState,
      };
    default:
      return state;
  }
};

export default TasksReducer;
