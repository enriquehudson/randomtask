import axios from 'axios';
const FormData = require('form-data');

type responseEmail = {
  email: string;
  status: string;
};

const useSendComment = () => {
  const sendComment = async (
    name: string,
    email: string,
    message: string,
  ): Promise<responseEmail> => {
    let data = new FormData();
    let res;
    data.append('name', name);
    data.append('email', email);
    data.append('message', message);
    await axios({
      method: 'post',
      url: 'https://randomtask.xyz/app/sendmail.php',
      data: data,
      headers: {'Content-Type': 'multipart/form-data'},
    })
      .then(function (response) {
        console.log('aqui', JSON.stringify(response.data));
        res = response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
    return res;
  };

  return {sendComment};
};

export default useSendComment;
