import React, { useState } from 'react';

import API from './functionsAPI';

import { loadProjects, loadTasks, clearState } from '../Context/TaskCreators';
import Spinner from 'react-native-loading-spinner-overlay';
import { useNavigation } from '@react-navigation/native';
import { Box, Button, Divider, HStack, Text, VStack } from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faDoorOpen } from '@fortawesome/free-solid-svg-icons/faDoorOpen';
import { faInfo } from '@fortawesome/free-solid-svg-icons/faInfo';
import { faCheck } from '@fortawesome/free-solid-svg-icons/faCheck';
import { faDatabase } from '@fortawesome/free-solid-svg-icons/faDatabase';
import { TodoistApi } from '@doist/todoist-api-typescript';
import { useTranslation } from 'react-i18next';
import { useTasksContext } from '../Context/TasksProvider';
import '../Utils/i18n';

const Config = () => {
  const { tasksState, tasksDispatch } = useTasksContext();
  const [showSpinner, toggleShowSpinner] = useState<boolean>(false);
  const navigation = useNavigation();
  const { t, i18n } = useTranslation();

  const api = new TodoistApi(tasksState?.token);

  const removeApiKey = () => {
    console.log('en removeApiKey');
    API.removeData('key');
    API.removeData('tasks');
    API.removeData('projects');
    navigation.goBack();
    tasksDispatch(clearState());
  };

  const refreshTasks = async () => {
    toggleShowSpinner(true);
    await API.removeData('tasks');
    await api
      .getTasks()
      .then((tasks) => {
        tasksDispatch(loadTasks(tasks));
        API.storeData('tasks', JSON.stringify(tasks));
      })
      .catch((error) => console.log(error));
    toggleShowSpinner(false);
  };

  const refreshProjects = async () => {
    toggleShowSpinner(true);
    await API.removeData('projects');
    await api
      .getProjects()
      .then((projects) => {
        tasksDispatch(loadProjects(projects));
        API.storeData('projects', JSON.stringify(projects));
      })
      .catch((error) => console.log(error));
    toggleShowSpinner(false);
  };

  return (
    <>
      <Spinner
        visible={showSpinner}
        textContent={t('randomScreen.loading') + ' '}
      />
      <Box w="100%">
        <VStack space={3} divider={<Divider />}>
          <Text fontSize="20" fontWeight="bold" marginLeft="2">
            API
          </Text>
          <HStack alignItems="center">
            <Box
              marginLeft="2"
              marginRight="2"
              borderRadius="md"
              backgroundColor="green.500"
              w="7"
              h="7"
              justifyContent="center"
              alignItems="center">
              <FontAwesomeIcon size={18} icon={faInfo} color="white" />
            </Box>
            <Text w="85%" fontSize="12">
              {tasksState.token}
            </Text>
          </HStack>

          <HStack alignItems="center">
            <Box
              marginLeft="2"
              marginRight="2"
              borderRadius="md"
              backgroundColor="gray.500"
              w="7"
              h="7"
              justifyContent="center"
              alignItems="center">
              <FontAwesomeIcon size={18} icon={faDoorOpen} color="white" />
            </Box>
            <HStack justifyContent="space-between" alignItems="center" w="80%">
              <Text>{t('configuration.lblDeleteAPIKey')}</Text>
              <Button size="sm" rounded="full" onPress={() => removeApiKey()}>
                {t('configuration.btnDeleteAPIKey')}
              </Button>
            </HStack>
          </HStack>

          <Text fontSize="20" fontWeight="bold" marginLeft="2">
            {t('configuration.titleTask')}
          </Text>

          <HStack alignItems="center">
            <Box
              marginLeft="2"
              marginRight="2"
              borderRadius="md"
              backgroundColor="red.500"
              w="7"
              h="7"
              justifyContent="center"
              alignItems="center">
              <FontAwesomeIcon size={16} icon={faDatabase} color="white" />
            </Box>
            <HStack justifyContent="space-between" alignItems="center" w="80%">
              <Text>
                {t('configuration.lblTotalProjects', {
                  projects: tasksState.projects?.length,
                })}
              </Text>
              <Button
                size="sm"
                rounded="full"
                onPress={() => refreshProjects()}>
                {t('configuration.btnRefreshProjects')}
              </Button>
            </HStack>
          </HStack>
          <HStack alignItems="center">
            <Box
              marginLeft="2"
              marginRight="2"
              borderRadius="md"
              backgroundColor="blue.500"
              w="7"
              h="7"
              justifyContent="center"
              alignItems="center">
              <FontAwesomeIcon size={18} icon={faCheck} color="white" />
            </Box>
            <HStack justifyContent="space-between" alignItems="center" w="80%">
              <Text>
                {t('configuration.lblTotalTasks', {
                  tasks: tasksState.tasks.length,
                })}
              </Text>

              <Button size="sm" rounded="full" onPress={() => refreshTasks()}>
                {t('configuration.btnRefresh')}
              </Button>
            </HStack>
          </HStack>
        </VStack>
      </Box>
    </>
  );
};

export default Config;
