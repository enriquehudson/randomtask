import AsyncStorage from '@react-native-community/async-storage';

const API = {
  async storeData(key: string, value: string) {
    try {
      console.log('en el set');
      await AsyncStorage.setItem(key, value);
      return true;
    } catch (e) {
      console.log('Error store data ' + key + ' ' + e);
      return false;
    }
  },
  async getData(key: string) {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        //console.log(value);
        return value;
      }
      return '';
    } catch (e) {
      return '';
    }
  },
  async removeData(key: string) {
    try {
      const value = await AsyncStorage.removeItem(key);
      console.log('remove ' + value);
    } catch (e) {
      console.log(e);
    }
  },
  deleteTaskList(key: string) {},
  editTaskList(key: string, action: string) {
    console.log(key, ' ' + action);
    return true;
  },
};
export {API as default};
