import React, { useState } from 'react';
import { Linking } from 'react-native';
import API from './functionsAPI';
import { setToken } from '../Context/TaskCreators';
import { Box, Button, Center, Input, Text, VStack } from 'native-base';
import { useTranslation } from 'react-i18next';
import '../Utils/i18n';
import { useTasksContext } from '../Context/TasksProvider';

const CaptureAPI = () => {
  const [apiKey, setApiKey] = useState<string>('');
  const { t } = useTranslation();

  const { tasksDispatch } = useTasksContext();

  const saveKey = async () => {
    console.log(apiKey);
    try {
      await API.storeData('key', apiKey);
      tasksDispatch(setToken(apiKey));
    } catch (e) {
      console.log('Error en el storage ' + e);
    }
  };

  return (
    <Box backgroundColor={'blue.100'} margin={1} padding={5}>
      <VStack>
        <Text fontSize="18" fontWeight="bold">
          {t('captureApi.lblSetAPIKey')}
        </Text>

        <Input
          onChangeText={(apiKeyTxt) => setApiKey(apiKeyTxt)}
          marginTop={4}
          size="lg"
          placeholder="API Key"
        />

        <Center marginTop={4}>
          <Button w="40%" borderRadius="full" onPress={() => saveKey()}>
            {t('captureApi.btnInit')}
          </Button>
        </Center>
        <VStack>
          <Text marginTop={5}>{t('captureApi.lblFoundApi')}</Text>
          <Button
            variant="link"
            onPress={() =>
              Linking.openURL('https://todoist.com/app/settings/integrations/developer')
            }>
            <Text>https://todoist.com/app/settings/integrations/developer</Text>
          </Button>
        </VStack>
        <Text marginTop={2} color="red.500" fontSize={16}>
          {t('captureApi.lblAdvertise')}
        </Text>
        <Button
          variant="link"
          onPress={() =>
            Linking.openURL('https://www.gsiser.com.mx/random-task-app/')
          }>
          <Text>{t('captureApi.linkPrivacyPolicy')}</Text>
        </Button>
      </VStack>
    </Box>
  );
};

export default CaptureAPI;
