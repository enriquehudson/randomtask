import React, { useEffect, useLayoutEffect, useState } from 'react';
import { Box, Button, Checkbox, Column, IconButton, Row, ScrollView, Text } from 'native-base';
import { faCopy } from '@fortawesome/free-solid-svg-icons/faCopy';
import { faShuffle } from '@fortawesome/free-solid-svg-icons/faShuffle';
import { faRefresh } from '@fortawesome/free-solid-svg-icons/faRefresh';
import { faClose } from '@fortawesome/free-solid-svg-icons/faClose';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { useTranslation } from 'react-i18next';
import { TaskType, ProjectType } from '../Utils/types';
import Clipboard from '@react-native-clipboard/clipboard';
import { useTasksContext } from '../Context/TasksProvider';
import Task from './Task';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

const ListTasks = ({ route }: { route: any }) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const [data, setData] = useState<TaskType[]>([]);
  const [showDetails, setShowDetails] = useState<boolean>(true);
  const [selectedTask, setSelectedTask] = useState<TaskType | null>(null);
  const { tasksState } = useTasksContext();
  const tasks = tasksState.filteredTasks;

  const shuffle = () => {
    const tasks = [...data].sort(() => Math.random() - 0.5);
    setData(tasks);
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <IconButton
          onPress={() => { navigation.goBack(); setSelectedTask(null); }}
          icon={<FontAwesomeIcon icon={faClose} size={20} />}
        />
      ),
    });
  }, [navigation]);

  useEffect(() => {
    const aux = tasks.map((item: TaskType) => {
      item = {
        ...item,
        projectName: tasksState.projects.find((proj: ProjectType) => proj && proj.id === item.projectId)?.name || 'Project Not Found',
      }
      return item;
    });
    setData(aux);
    setSelectedTask(null);
  }, [tasks]);

  const copyToClipboard = () => {
    const textTask = showDetails ?
      data.map(item => {
        if (item.due?.isRecurring === true) {
          return `${item.content}, is Recurring, ${item.projectName}`;
        } else {
          return `${item.content}, ${item.projectName}`;
        }
      }).join("\n") :
      data.map(item => item.content).join("\n");
    Clipboard.setString(textTask);
  };

  return (
    <>
      <Box marginY="0.5" marginX="1" flex="1" justifyContent="center" rounded="lg" borderColor="gray.600" borderWidth="2">
        <Row roundedTop="lg" justifyContent="space-between" backgroundColor="blue.200">
          <Row marginLeft={2} alignItems="center" width="50%" >
            <Checkbox value="showDetails" onChange={() => setShowDetails(!showDetails)} isChecked={showDetails} accessibilityLabel="Show Details">
              {t('taskList.showDetails')}
            </Checkbox>
          </Row>
          <Button variant='link' size='sm' onPress={shuffle} endIcon={<FontAwesomeIcon icon={faShuffle} size={12} />}>
            {t('taskList.shuffle')}
          </Button >
          <Button variant='link' size='sm' marginRight="1" onPress={copyToClipboard} endIcon={<FontAwesomeIcon icon={faCopy} size={12} />}>
            {t('taskList.copy')}
          </Button>
        </Row>
        <Row
          backgroundColor="amber.100"
          margin="1"
          flex="1"
        >
          <ScrollView>
            {data.map((item: TaskType, index: number) => {
              return showDetails ? (
                <TouchableOpacity key={item.id} onPress={() => setSelectedTask(item)}>
                  <Row key={item.id} alignItems="center">
                    <Text fontSize={12} width="82%">{`${index + 1}: ${item.content}`}</Text>
                    <Box width={2} alignItems="center">
                      {item.due?.isRecurring &&
                        <FontAwesomeIcon icon={faRefresh} size={8} />}
                    </Box>
                    <Text marginLeft={1} fontSize={12}>{item.projectName}</Text>
                  </Row>
                </TouchableOpacity>
              ) : (
                <Row key={item.id}>
                  <Text>{index + 1}: </Text>
                  <Text>{item.content}</Text>
                </Row>
              )
            })}
          </ScrollView>
        </Row>
      </Box>
      {selectedTask && <Task content={selectedTask} />}
    </>
  );
};

export default ListTasks;
