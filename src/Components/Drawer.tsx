import React from 'react';
import {
  Box,
  VStack,
  Button,
  Text,
  Divider,
  Image,
  Center,
} from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faGear } from '@fortawesome/free-solid-svg-icons/faGear';
import { faInfo } from '@fortawesome/free-solid-svg-icons/faInfo';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope';
import { useTranslation } from 'react-i18next';
import '../Utils/i18n';

const DrawerView = ({ navigation }: any) => {
  const { t } = useTranslation();
  return (
    <VStack divider={<Divider />}>
      <Box
        justifyContent="center"
        alignItems="center"
        h="150"
        backgroundColor="#4c91ff">
        <Text fontSize={24} fontWeight="bold" marginTop={2}>
          Random Task
        </Text>
        <Text>{t('drawer.subTitle')}</Text>
      </Box>
      <Button
        size="lg"
        leftIcon={<FontAwesomeIcon icon={faEnvelope} />}
        justifyContent="flex-start"
        variant="ghost"
        onPress={() => navigation.navigate('Contact')}>
        {t('drawer.contact')}
      </Button>

      <Button
        size="lg"
        justifyContent="flex-start"
        leftIcon={<FontAwesomeIcon icon={faInfo} />}
        variant="ghost"
        onPress={() => navigation.navigate('About')}>
        {t('drawer.about')}
      </Button>

      <Button
        size="lg"
        justifyContent="flex-start"
        leftIcon={<FontAwesomeIcon icon={faGear} />}
        variant="ghost"
        onPress={() => navigation.navigate('Config')}>
        {t('drawer.configuration')}
      </Button>

      <Center>
        <Image
          source={require('../../assets/logo.png')}
          alt="Logo App"
          width="100"
          height="1/2"
          resizeMode="contain"
        />
      </Center>
    </VStack>
  );
};

export default DrawerView;
