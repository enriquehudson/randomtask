/* @flow */

import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
// import {
//   Header,
//   Text,
//   Title,
//   Left,
//   Body,
//   Right,
//   Button,
//   Icon,
// } from 'native-base';
import {useNavigation} from '@react-navigation/native';

const Detail = () => {
  const navigation = useNavigation();
  return (
    <View>
      <View>
        <View>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            {/* <Icon name="arrow-back" /> */}
          </TouchableOpacity>
        </View>
        <View>
          <Text>Random Task</Text>
        </View>
        <View />
      </View>
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>Details Screen</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default Detail;
