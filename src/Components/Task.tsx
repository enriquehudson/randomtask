import React, { useEffect, useState } from 'react';

import { deleteTask, changeDate, deleteDate } from '../Context/TaskCreators';
import moment from 'moment';
import 'moment/locale/es';
import 'moment/locale/de';
import 'moment/locale/fr';
import Spinner from 'react-native-loading-spinner-overlay';
import { Box, Button, Text, Stack, Center, Row, Column } from 'native-base';
import { TaskType } from '../Utils/types';
import { useTranslation } from 'react-i18next';
import '../Utils/i18n';
import { TodoistApi } from "@doist/todoist-api-typescript"
import { useTasksContext } from '../Context/TasksProvider';

const Task = ({ content }: { content: TaskType }) => {
  const { tasksState, tasksDispatch } = useTasksContext();
  const [showSpinner, toggleShowSpinner] = useState<boolean>(false);
  const [randomDays, setRandomDays] = useState<number>(2);
  const { t, i18n } = useTranslation();

  const api = new TodoistApi(tasksState?.token);

  const completeTasks = async () => {
    toggleShowSpinner(true);
    api.closeTask(content.id)
      .then(() => tasksDispatch(deleteTask(content.id)))
      .catch((error) => console.log(error))
      .finally(() => toggleShowSpinner(false));
  };

  const moveToday = async () => {
    const today = moment().format('YYYY-MM-DD');
    toggleShowSpinner(true);
    api.updateTask(content.id.toString(), {
      dueDate: today,
      ...(content.due?.isRecurring && { dueString: content.due.string })
    })
      .then(() => tasksDispatch(
        changeDate(content.id, 'date', today)
      ))
      .catch((error) => console.log(error))
      .finally(() => toggleShowSpinner(false));
  };

  const moveXDays = async (days: number) => {
    const dateDays = moment().add(days, 'days').format('YYYY-MM-DD');
    toggleShowSpinner(true);
    api.updateTask(content.id.toString(), {
      dueDate: dateDays,
      ...(content.due?.isRecurring && { dueString: content.due.string })
    })
      .then(() => tasksDispatch(
        changeDate(content.id, 'date', dateDays),
      ))
      .catch((error) => console.log(error))
      .finally(() => toggleShowSpinner(false));
  };

  const removeTask = async () => {
    toggleShowSpinner(true);
    api.deleteTask(content.id.toString())
      .then(() => tasksDispatch(deleteTask(content.id)))
      .catch((error) => console.log(error))
      .finally(() => toggleShowSpinner(false));
  };

  const removeDate = async () => {
    toggleShowSpinner(true);
    api.updateTask(content.id.toString(), { dueString: "no date" })
      .then(() => tasksDispatch(deleteDate(content.id)))
      .catch((error) => console.log(error))
      .finally(() => toggleShowSpinner(false));
  };

  useEffect(() => {
    setRandomDays(Math.floor(Math.random() * 15) + 2);
  }, [content])

  return (
    <Box p="1" >
      <Spinner visible={showSpinner} textContent={t('randomScreen.loading') + ' '} />
      <Box
        rounded="lg"
        borderColor="coolGray.600"
        borderWidth="2"
      >
        <Column
          backgroundColor="blue.200"
          roundedTop="lg"
          p="1"
          borderBottomWidth={1}>
          <Row justifyContent="space-between">
            <Row>
              <Text bold>{t('randomScreen.project')} </Text>
              <Text>{content.projectName}</Text>
            </Row>
            <Row>
              <Text bold>{t('task.priority')}</Text>
              <Text>{content.priority}</Text>
            </Row>
          </Row>
          <Row justifyContent="space-between">
            {content.due && (
              <Row>
                <Text bold>{t('task.expires')}</Text>
                <Text>
                  {moment(content.due.date).locale(i18n.language).format('LL')}
                </Text>
              </Row>
            )}
            {content.due && content.due.isRecurring && (
              <Row>
                <Text bold>{t('task.repeats')}</Text>
                <Text>{content.due.string}</Text>
              </Row>
            )}
          </Row>
        </Column>
        <Center w="full" alignContent="center" paddingX={0.5}>
          <Text textAlign="center" fontSize="xl" bold>
            {content.content}
          </Text>
          {content.description.length > 0 && <Text>{content.description}</Text>}
          {content.labels.length > 0 && (
            <Row>
              <Text bold>{t('task.labels')}</Text>
              <Text>{content.labels}</Text>
            </Row>
          )}
        </Center>

        <Stack direction="row" flexWrap="wrap" mb="2" justifyContent="center">
          <Button width="47%" margin="1" onPress={() => completeTasks()}>
            {t('task.btnComplete')}
          </Button>
          <Button width="47%" margin="1" onPress={() => moveXDays(1)}>
            {t('task.btnPostpone1')}
          </Button>
          <Button width="47%" margin="1" onPress={() => moveToday()}>
            {t('task.btnToday')}
          </Button>
          <Button width="47%" margin="1" onPress={() => removeTask()}>
            {t('task.btnDelete')}
          </Button>
          <Button width="47%" margin="1" onPress={() => removeDate()}>
            {t('task.btnRemoveDate')}
          </Button>
          <Button width="47%" margin="1" onPress={() => moveXDays(randomDays)}>
            {t('task.postponeX', { days: 'X' })}
          </Button>
        </Stack>
      </Box>
    </Box>
  );
};

export default Task;
