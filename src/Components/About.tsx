import React from 'react';
import { Center, Image, Text, VStack } from 'native-base';
import { useTranslation } from 'react-i18next';

import '../Utils/i18n';

const About = () => {
  const { t, i18n } = useTranslation();

  return (
    <VStack margin="3">
      <Center>
        <Text fontSize="24" fontWeight="bold">
          Random Task
        </Text>
      </Center>
      <Text textAlign="justify" marginTop="4">
        {t('about.paragraph1')}
      </Text>
      <Text textAlign="justify" marginTop="4">
        {t('about.paragraph2')}
      </Text>
      <Center marginTop={10}>
        <Image
          source={require('../../assets/tasks.png')}
          alt="Envelope"
          size="2xl"
          resizeMode="contain"
        />
      </Center>
    </VStack>
  );
};

export default About;
