import React, { useEffect, useState } from 'react';
import { Alert } from 'react-native';

import { useNavigation } from '@react-navigation/core';
import Spinner from 'react-native-loading-spinner-overlay';
import useSendComment from '../Hooks/useSendComment';
import {
  Box,
  Button,
  Center,
  Divider,
  Image,
  Input,
  Text,
  TextArea,
  VStack,
} from 'native-base';
import { useTranslation } from 'react-i18next';

import '../Utils/i18n';

const Contact = () => {
  const navigation = useNavigation();
  const { t } = useTranslation();
  const [name, setName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [message, setMessage] = useState<string>('');
  const [disableButton, setDisableButton] = useState<boolean>(true);
  const [validEmail, setValidEmail] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { sendComment } = useSendComment();

  const handleComment = async () => {
    setIsLoading(true);

    const res = await sendComment(name, email, message);
    console.log('aca', res);
    if (res.status === 'success') {
      Alert.alert(
        t('contactForm.successTitle'),
        t('contactForm.successMessage').toString(),
        [
          {
            text: t('contactForm.okButton').toString(),
            onPress: () => {
              setIsLoading(false);
              setName('');
              setMessage('');
              setEmail('');
              navigation.goBack();
            },
          },
        ],
      );
    } else {
      Alert.alert(
        t('contactForm.errorTitle'),
        t('contactForm.errorMessage').toString(),
        [
          {
            text: t('contactForm.okButton').toString(),
            onPress: () => setIsLoading(false),
            style: 'cancel',
          },
        ],
      );
    }
  };

  useEffect(() => {
    let regex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    setValidEmail(regex.test(email));
  }, [email]);

  useEffect(() => {
    if (validEmail && name && message) {
      setDisableButton(false);
    } else {
      setDisableButton(true);
    }
  }, [validEmail, name, message]);

  return (
    <Box>
      <Spinner
        visible={isLoading}
        textContent={t('contactForm.loaderLabel').toString()}
      />
      <VStack margin={2}>
        <Text fontSize={16} fontWeight="bold" margin={2}>
          {t('contactForm.sendAMessage')}
        </Text>
        <Text textAlign="justify" margin={2}>
          {t('contactForm.sendDescription')}
        </Text>
        <VStack space={2}>
          <Text bold marginTop={1}>
            {t('contactForm.lblName')}:
          </Text>
          <Input
            variant="outline"
            placeholder={t('contactForm.lblName').toString()}
            value={name}
            onChangeText={(txt: string) => setName(txt)}
          />
          <Text bold marginTop={1}>
            {t('contactForm.lblEmail')}:
          </Text>
          <Input
            variant="outline"
            placeholder={t('contactForm.lblEmail').toString()}
            value={email}
            autoCapitalize={'none'}
            onChangeText={(txt: string) => setEmail(txt)}
          />
          <Text bold marginTop={1}>
            {t('contactForm.lblMessage')}:
          </Text>
          <TextArea
            numberOfLines={15}
            autoCompleteType={false}
            placeholder={t('contactForm.lblMessage').toString()}
            value={message}
            onChangeText={(txt: string) => setMessage(txt)}
          />

          <Center>
            <Button
              size="lg"
              rounded="full"
              isDisabled={disableButton}
              onPress={() => handleComment()}>
              {t('contactForm.btnSend')}
            </Button>
            <Image
              source={require('../../assets/envelope.png')}
              alt="Envelope"
              size="xl"
              resizeMode="contain"
            />
          </Center>
        </VStack>
      </VStack>
    </Box>
  );
};

export default Contact;
