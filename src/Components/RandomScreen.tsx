import React, { useState, useEffect, useLayoutEffect, useCallback } from 'react';
import { Box, Button, Center, IconButton, Row, Select, Text } from 'native-base';
import { useTranslation } from 'react-i18next';

import '../Utils/i18n';
import API from './functionsAPI';
import Task from './Task';

import { TodoistApi } from "@doist/todoist-api-typescript"

import {
  loadProjects,
  loadTasks,
  filterTasks,
  clearState,
} from '../Context/TaskCreators';
import Spinner from 'react-native-loading-spinner-overlay';
import { useNavigation } from '@react-navigation/native';
import { ProjectType, TaskType, FiltersDate } from '../Utils/types';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faRefresh } from '@fortawesome/free-solid-svg-icons/faRefresh';
import { Alert } from 'react-native';
import { useTasksContext } from '../Context/TasksProvider';

const RandomScreen = () => {
  const navigation = useNavigation();
  const { t, i18n } = useTranslation();

  const { tasksState, tasksDispatch } = useTasksContext();
  const [showSpinner, toggleShowSpinner] = useState<boolean>(false);
  const [isFirstLoad, setIsFirstLoad] = useState<boolean>(true);
  const apiKey = tasksState?.token;
  const api = new TodoistApi(apiKey);

  const getProjects = useCallback(async () => {
    let projectsStorage = await API.getData('projects');
    let projectsResponse: ProjectType[] = [];
    if (projectsStorage === '') {
      await api.getProjects().then((projects) => {
        projectsResponse = projects;
      }).catch((error) => console.log(error));

      if (projectsResponse[0].error) {
        API.removeData('key');
        tasksDispatch(clearState());
      } else {
        await API.storeData('projects', JSON.stringify(projectsResponse));
      }
    } else {
      projectsResponse = JSON.parse(projectsStorage);
    }
    tasksDispatch(loadProjects(projectsResponse));
  }, [apiKey, tasksDispatch]);

  const getTasks = useCallback(async () => {
    let taskStore = await API.getData('tasks');
    let tasksResponse: any = [];

    if (taskStore === '' || taskStore === '[]') {
      await api.getTasks().then((tasks) => tasksResponse = tasks).catch((error) => Alert.alert('Something was wrong. Try later again', error.message));
    } else {
      const tasksRes = JSON.parse(taskStore);
      if (typeof tasksRes[0].isCompleted === 'undefined') {
        await api.getTasks().then((tasks) => tasksResponse = tasks).catch((error) => Alert.alert('Something was wrong. Try later again', error.message));
      } else {
        tasksResponse = tasksRes;
      }
    }
    tasksDispatch(loadTasks(tasksResponse));
    API.storeData('tasks', JSON.stringify(tasksResponse));
  }, [apiKey, tasksDispatch]);

  const changeProject = async (value: string) => setProjectSelected(value);

  const changeFilterDate = async (value: string) =>
    setDateFilterSelected(value);

  const changePriorityFilter = async (value: string) =>
    setPriorityFilter(value);

  const getFunctionFilter = (filter: string) => {
    const todayDate = moment().format('YYYY-MM-DD');
    const tomorrowDate = moment().add(1, 'days').format('YYYY-MM-DD');
    const yesterdayDate = moment().add(-1, 'days').format('YYYY-MM-DD');

    switch (filter) {
      case FiltersDate.TODAY:
        return (item: TaskType) =>
          item.due !== null && item.due !== null && item.due.date === todayDate;

      case FiltersDate.TODAY:
        return (item: TaskType) =>
          item.due !== null && item.due !== null && item.due.date === todayDate;

      case FiltersDate.TOMORROW:
        return (item: TaskType) =>
          item.due !== null && item.due.date === tomorrowDate;

      case FiltersDate.TOMORROWANDTODAY:
        return (item: TaskType) =>
          item.due !== null &&
          (item.due?.date === todayDate || item.due.date === tomorrowDate);

      case FiltersDate.EXPIRED:
        return (item: TaskType) =>
          item.due !== null && item.due.date < todayDate;

      case FiltersDate.TODAYANDEXPIRED:
        return (item: TaskType) =>
          item.due !== null && item.due.date <= todayDate;

      case FiltersDate.TODAYANDAFTER:
        return (item: TaskType) =>
          item.due !== null && item.due.date >= todayDate;

      case FiltersDate.TODAYANDYESTERDAY:
        return (item: TaskType) =>
          item.due !== null &&
          (item.due?.date === todayDate || item.due.date === yesterdayDate);

      case FiltersDate.YESTERDAY:
        return (item: TaskType) =>
          item.due !== null && item.due?.date === yesterdayDate;

      case FiltersDate.WITHEXPIRATIONDATE:
        return (item: TaskType) => item.due !== null;

      case FiltersDate.WITHOUTEXPIRATIONDATE:
        return (item: TaskType) => item.due === null;
    }
  };

  const getRandomTask = async () => {
    if (tasksState.filteredTasks.length > 0) {
      const item: TaskType =
        tasksState.filteredTasks[
        Math.floor(Math.random() * tasksState.filteredTasks.length)
        ];

      const project = tasksState.projects.find((proj: ProjectType) =>
        item.projectId === proj.id
      );
      if (project) {
        item.projectName = project.name;
      } else {
        item.projectName = '';
      }
      await setTaskShow(item);
    }
  };

  const [projectSelected, setProjectSelected] = useState<string>('0');
  const [dateFilterSelected, setDateFilterSelected] = useState<string>('today');
  const [priorityFilter, setPriorityFilter] = useState<string>('0');
  const [taskShow, setTaskShow] = useState<TaskType | null>(null);

  const applyFilters = useCallback(() => {
    let aux: TaskType[];

    aux = tasksState.tasks.filter((item: TaskType) => item.isCompleted === false);

    if (projectSelected !== '0') {
      aux = aux.filter((item: TaskType) => item.projectId === projectSelected);
    }
    if (dateFilterSelected !== 'all') {
      let functionFilter: any = getFunctionFilter(dateFilterSelected);
      aux = aux.filter(functionFilter);
    }
    if (priorityFilter !== '0') {
      aux = (priorityFilter === '5') ? aux.filter((item: TaskType) => item.priority > 1) : aux.filter((item: TaskType) => item.priority === parseInt(priorityFilter, 10));
    }

    tasksDispatch(filterTasks(aux));
  }, [
    tasksState.tasks,
    projectSelected,
    dateFilterSelected,
    priorityFilter,
    tasksDispatch,
  ]);

  useEffect(() => {
    applyFilters();
  }, [applyFilters]);

  useLayoutEffect(() => {
    const refreshTasks = async () => {
      toggleShowSpinner(true);
      await API.removeData('tasks');
      await getTasks();
      toggleShowSpinner(false);
    };

    navigation.setOptions({
      headerRight: () => (
        <IconButton
          variant="ghost"
          onPress={refreshTasks}
          icon={<FontAwesomeIcon icon={faRefresh} size={20} color="#FFF" />}
        />
      ),
    });
  }, [navigation, getTasks]);

  const firstLoad = async () => {
    toggleShowSpinner(true);
    await getProjects();
    await getTasks();
    toggleShowSpinner(false);
  }

  useEffect(() => {
    if (isFirstLoad) {
      firstLoad();
      setIsFirstLoad(false);
    }
  }, [isFirstLoad, getProjects, getTasks, setIsFirstLoad]);

  const lblAll = t('randomScreen.filters.all');

  return (
    <>
      <Spinner
        visible={showSpinner}
        color="blue"
        textContent={t('randomScreen.loading').toString()}
      />
      <Box p="1">
        <Box
          rounded="lg"
          shadow="2"
          borderColor="coolGray.600"
          borderWidth="2"
          p="2"
          bgColor="blue.200">
          <Text fontSize={18} margin={0.5} fontWeight="bold">
            {t('randomScreen.totalTask', { tasks: tasksState.tasks.length })}
          </Text>
          <Row justifyContent="space-between">
            <Text fontSize={18} margin={0.5} fontWeight="bold">
              {t('randomScreen.selectedTasks', {
                tasks: tasksState.filteredTasks.length,
              })}
            </Text>
            <Button
              variant="link"
              isDisabled={tasksState.filteredTasks.length === 0}
              onPress={() => navigation.navigate('List' as never)}>
              {t('randomScreen.list')}
            </Button>
          </Row>
          <Text fontSize={16} fontWeight="bold">
            {t('randomScreen.project')}
          </Text>
          <Select
            variant="rounded"
            placeholder="Select Project"
            selectedValue={projectSelected}
            onValueChange={(project) => changeProject(project)}>
            <Select.Item label={lblAll} value="0" />
            {Object.keys(tasksState.projects).map((key: string) => {
              return (
                <Select.Item
                  label={tasksState.projects[parseInt(key, 10)].name}
                  value={tasksState.projects[parseInt(key, 10)].id}
                  key={tasksState.projects[parseInt(key, 10)].id}
                />
              );
            })}
          </Select>

          <Text fontSize={16} fontWeight="bold">
            {t('randomScreen.dateFilter')}
          </Text>
          <Select
            placeholder={t('randomScreen.project') + ' '}
            variant="rounded"
            selectedValue={dateFilterSelected}
            onValueChange={(dfs) => changeFilterDate(dfs)}>
            {Object.values(FiltersDate).map((item) => (
              <Select.Item
                label={t('randomScreen.filters.' + item)}
                value={item}
                key={item}
              />
            ))}
          </Select>
          <Text fontSize={16} fontWeight="bold">
            {t('randomScreen.priorityFilter')}
          </Text>
          <Select
            variant="rounded"
            placeholder={t('randomScreen.priorityFilter') + ' '}
            selectedValue={priorityFilter}
            onValueChange={(priority) => changePriorityFilter(priority)}>
            <Select.Item label={lblAll} value="0" />
            <Select.Item
              label={t('randomScreen.filters.moreThan1')}
              value="5"
            />
            <Select.Item label="1" value="1" />
            <Select.Item label="2" value="2" />
            <Select.Item label="3" value="3" />
            <Select.Item label="4" value="4" />
          </Select>
          <Center>
            <Button rounded="full" width="75%" onPress={() => getRandomTask()}>
              {t('randomScreen.btnRandomTask')}
            </Button>
          </Center>
        </Box>
      </Box >
      {!!taskShow?.content && <Task content={taskShow} />
      }
    </>
  );
};

export default RandomScreen;
