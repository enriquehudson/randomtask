import React, { useEffect } from 'react';
import API from './functionsAPI';
import CaptureAPI from './CaptureAPI';
import RandomScreen from './RandomScreen';
import { setToken } from '../Context/TaskCreators';
import { useTasksContext } from '../Context/TasksProvider';

const Home = () => {
  const { tasksState, tasksDispatch } = useTasksContext();

  useEffect(() => {
    const init = async () => {
      try {
        const apiKey = await API.getData('key');
        if (apiKey !== '') {
          tasksDispatch(setToken(apiKey));
        }
      } catch (e) {
        console.log('error leyendo ' + e);
      }
    };
    init();
  }, [tasksDispatch]);

  return (
    <>
      {tasksState.token === '' ? (
        <CaptureAPI />
      ) : (
        <RandomScreen />
      )}
    </>
  );
};

export default Home;
