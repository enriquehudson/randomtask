import React from 'react';
import { StyleSheet } from 'react-native';

import { createDrawerNavigator } from '@react-navigation/drawer';
import 'react-native-gesture-handler';

import Home from './Components/Home';
import Detail from './Components/Detail';
import About from './Components/About';
import Contact from './Components/Contact';
import Config from './Components/Config';
import ListTasks from './Components/ListTasks';
import DrawerView from './Components/Drawer';

import { IconButton } from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faClose } from '@fortawesome/free-solid-svg-icons/faClose';

import { useTranslation } from 'react-i18next';
import './Utils/i18n';

const Navigator = () => {
  const { t, i18n } = useTranslation();
  const Drawer = createDrawerNavigator();

  const backButton = (navigation: any) => (
    <IconButton
      onPress={() => navigation.goBack()}
      icon={<FontAwesomeIcon icon={faClose} size={20} color='#FFF' />}
    />
  );

  return (
    <Drawer.Navigator
      drawerContent={(props) => <DrawerView {...props} />}
      screenOptions={{
        headerStyle: {
          backgroundColor: '#37acfa',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}
    >
      <Drawer.Screen
        name="Home"
        component={Home}
        options={() => ({
          title: 'Random Task',
          headerRight: () => null,
        })}
      />
      <Drawer.Screen
        name="Details"
        component={Detail}
        options={{ drawerLabel: 'details', headerTitleAlign: 'center' }}
      />
      <Drawer.Screen
        name="Contact"
        component={Contact}
        options={({ navigation }) => ({
          headerLeft: () => null,
          title: t('titles.contact').toString(),
          headerRight: () => backButton(navigation),
          headerTitleAlign: 'center',
          headerRightContainerStyle: styles.rightContainer,
        })}
      />
      <Drawer.Screen
        name="About"
        component={About}
        options={({ navigation }) => ({
          headerLeft: () => null,
          title: t('titles.about').toString(),
          headerTitleAlign: 'center',
          headerRight: () => backButton(navigation),
        })}
      />
      <Drawer.Screen
        name="List"
        component={ListTasks}
        initialParams={{ tasks: [] }}
        options={() => ({
          title: t('titles.taskList').toString(),
          headerStyle: {
            backgroundColor: '#4286fb',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
      />
      <Drawer.Screen
        name="Config"
        component={Config}
        options={({ navigation }) => ({
          headerLeft: () => null,
          title: t('titles.configuration').toString(),
          headerTitleAlign: 'center',
          headerRight: () => backButton(navigation),
        })}
      />
    </Drawer.Navigator>
  );
};

const styles = StyleSheet.create({
  backButton: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textBack: {
    marginLeft: 4,
  },
  leftContainer: {
    paddingLeft: 5,
  },
  rightContainer: {
    paddingRight: 5,
  },
});

export default Navigator;
