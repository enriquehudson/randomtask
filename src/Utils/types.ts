export interface StateReducer {
  token: string;
  tasks: TaskType[];
  projects: ProjectType[];
  filteredTasks: TaskType[];
  selectedTask: TaskType | null;
}

export interface ActionTask {
  type: string;
  id?: string;
  due?: object;
  token?: string;
  projects?: ProjectType[];
  value?: any;
  tasks?: TaskType[];
  option?: any;
}

export interface DueType {
  date: string;
  isRecurring?: boolean;
  lang?: string;
  string?: string;
  datetime?: string | null;
  timezone?: string | null;
}

export interface TaskType {
  order: number;
  id: string;
  url: string;
  content: string;
  commentCount: number;
  description: string;
  projectId: string;
  isCompleted: boolean;
  labels: string[];
  priority: number;
  createdAt: string;
  creatorId: string;
  projectName?: string;
  due: DueType | null;
}
export interface ProjectType {
  id: string;
  name: string;
  color: string;
  commentCount: number;
  isShared: boolean;
  isFavorite: boolean;
  url: string;
  isInboxProject: boolean;
  isTeamInbox: boolean;
  order: number;
  viewStyle: string;
  error?: any;
}

export enum FiltersDate {
  TODAY = 'today',
  TOMORROW = 'tomorrow',
  TOMORROWANDTODAY = 'tomorrowAndToday',
  ALL = 'all',
  EXPIRED = 'expired',
  TODAYANDEXPIRED = 'todayAndExpired',
  TODAYANDAFTER = 'todayAndAfter',
  TODAYANDYESTERDAY = 'todayAndYesterday',
  YESTERDAY = 'yesterday',
  WITHEXPIRATIONDATE = 'withExpirationDate',
  WITHOUTEXPIRATIONDATE = 'withoutExpirationDate',
}
