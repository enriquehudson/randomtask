import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import * as RNLocalize from 'react-native-localize';

import es from './locales/es.json';
import en from './locales/en.json';
import de from './locales/de.json';
import fr from './locales/fr.json';

const locales = RNLocalize.getLocales();
let lngCode = locales[0].languageCode;

const languages = {
  en: en,
  es: es,
  de: de,
  fr: fr,
};

if (Object.keys(languages).filter((val) => val === lngCode).length === 0) {
  lngCode = 'en';
}

i18n.use(initReactI18next).init({
  compatibilityJSON: 'v3',
  lng: lngCode,
  fallbackLng: lngCode,
  resources: languages,
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
