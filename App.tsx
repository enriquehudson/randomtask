/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';

import TasksProvider from './src/Context/TasksProvider';
import {NativeBaseProvider} from 'native-base';
import {NavigationContainer} from '@react-navigation/native';
import Navigator from './src/Navigator';
import 'react-native-get-random-values';

function App() {
  return (
    <>
      <TasksProvider>
        <NativeBaseProvider>
          <NavigationContainer>
            <Navigator />
          </NavigationContainer>
        </NativeBaseProvider>
      </TasksProvider>
    </>
  );
}

export default App;
